const board = document.querySelector('#board')
const colors = ['#e73c4a', '#ad44a4', '#2db1b6', '#ffa150', '#32fd87', '#eeff00', '#f1508e']
const squaresQuantity = 360

for (let i = 0; i < squaresQuantity; i++) {
  const square = document.createElement('div')

  square.classList.add('square')
  square.addEventListener('mouseover', () => setColor(square))
  square.addEventListener('mouseleave', () => removeColor(square))

  board.append(square)
}

function setColor(elem) {
  const color = getRandomColor()
  elem.style.backgroundColor = color
  elem.style.boxShadow = `0 0 2px ${color}, 0 0 10px ${color}`
}
function removeColor(elem) {
  elem.style.backgroundColor = '#1d1d1d'
  elem.style.boxShadow = `0 0 2px #000`
}
function getRandomColor() {
  const index = Math.floor(Math.random() * colors.length)
  return colors[index]
}